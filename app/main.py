"""
Setup of API endpoints
"""

from typing import Union
from fastapi import FastAPI
from pydantic import BaseModel
from app.data_tables import amino_acid_table, grantham_distance_table

DESCRIPTION = """
Lookup the Grantham distance between two amino acids

## Usage:

Either a web interface or an API
(TODO: Update when completed)

"""

tags_metadata = [
    {
        "name": "Info",
        "description": "Returns some information on a single amino acid."
    },
    {
        "name": "Grantham",
        "description": (
            "Returns the Grantham distance between two amino"
            "acids. Returns None/Null if residue not recognised"
        ),
    },
]

app = FastAPI(
    title="Grantham distance",
    description=DESCRIPTION,
    openapi_tags=tags_metadata,
    docs_url="/",
    version="0.1",
)


class GranthamDistance(BaseModel):
    """Response model for grantham distance"""

    amino_acid_1: str
    amino_acid_2: str
    grantham_distance: int


@app.get("/api/{amino_acid}", tags=["Info"])
def read_aa(amino_acid: str) -> dict:  # pragma: no cover
    """
    For testing - return a single amino acid.
    Possibly return some info about each aa?
    """
    return {"Amino acid": lookup_amino_acid(amino_acid)}


@app.get(
    "/api/{amino_acid_1}/{amino_acid_2}",
    response_model=GranthamDistance,
    tags=["Grantham"],
)
def read_two_aa(
    amino_acid_1: str, amino_acid_2: str
) -> GranthamDistance:  # pragma: no cover
    """Get both amino acids and return the Grantham distance"""
    grantham_distance = lookup_grantham_distance(amino_acid_1, amino_acid_2)
    return GranthamDistance(
        amino_acid_1=amino_acid_1,
        amino_acid_2=amino_acid_2,
        grantham_distance=grantham_distance,
    )


def lookup_amino_acid(amino_acid: str) -> str:
    """
    Look up the provided amino acid, which my be 1 letter, 3 letter,
    or full name, and return the 3 letter code
    """
    # Ensure consitent case usage.
    amino_acid = amino_acid.capitalize()

    # Since there's such a small number of possibilities,
    # we can probably safely hard code everything and just
    # loop through a list rather than try to use a dict for
    # every possible input

    for aa_codes in amino_acid_table:
        if amino_acid in aa_codes:
            # Return the 3-letter code (position 1 in tuple)
            return aa_codes[1]
    raise KeyError(f"Specified amino acid code '{amino_acid}' was not found.")


def lookup_grantham_distance(
    amino_acid_1: str, amino_acid_2: str
) -> Union[int, None]:
    """Lookup the Grantham distance between two amino acids"""
    # The same amino acid is always 0 distance
    if amino_acid_1 == amino_acid_2:
        return 0

    # Check the amino acids are correct
    try:
        amino_acid_1 = lookup_amino_acid(amino_acid_1)
    except KeyError:
        pass
    try:
        amino_acid_2 = lookup_amino_acid(amino_acid_2)
    except KeyError:
        pass

    # Now do the lookup
    # This will need to account for the arrangement of the table,
    # which means some times we will have to look up twice - along
    # both axes
    try:
        return grantham_distance_table[amino_acid_1][amino_acid_2]
    except KeyError:
        # Swap the order of the axes
        try:
            return grantham_distance_table[amino_acid_2][amino_acid_1]
        except KeyError:
            return None
