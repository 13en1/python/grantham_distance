# Grantham score lookup

Just a very simple website to lookup the Grantham score of two amino acids.

Probably wants an actual webpage interface, rather than just an API, but maybe
also an API? Just for the fun of it.

Grantham score matrix can be found here:
https://gist.github.com/danielecook/501f03650bca6a3db31ff3af2d413d2a


## First iteration

Uses FastAPI to present a simple API-based grantham score finder
Just use with /amino_acid_1/amino_acid_2/ and the return will be
a JSON file.

### Docker building and deployment

TODO: Add to Gitlub and setup CI with pylint, flake8, mypy, pytest, etc.
      Can these all be added to the Makefile too?

TODO: Deploy to Heroku in container mode