"""
Tests for the Grantham score API
"""

import pytest
from app.main import (read_aa,
                      read_two_aa,
                      lookup_amino_acid,
                      lookup_grantham_distance)


def test_lookup_aa_1_letter():
    """Test some single amino acids from various -> 3 letter code"""
    assert lookup_amino_acid("r") == "Arg"
    assert lookup_amino_acid("leu") == "Leu"
    assert lookup_amino_acid("valine") == "Val"
    assert lookup_amino_acid("His") == "His"


def test_lookup_incorrect():
    """If amino acid is incorrect, return a KeyError"""
    with pytest.raises(KeyError):
        lookup_amino_acid("Foo")


def test_lookup_grantham_distance_same():
    """Same amino acids should have a score of 0"""
    assert lookup_grantham_distance("Ile", "Ile") == 0


def test_lookup_grantham_distance():
    """Check that some grantham distances are retrieved correctly"""
    assert lookup_grantham_distance("Arg", "Cys") == 180


def test_lookup_grantham_distance_reverse():
    """This lookup will fail on the default axis"""
    assert lookup_grantham_distance("Gly", "Thr") == 59

def test_lookup_grantham_distance_wrong():
    """This lookup will fail on the default axis"""
    assert lookup_grantham_distance("Gly", "Foo") == None
